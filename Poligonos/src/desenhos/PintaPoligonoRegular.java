package desenhos;


import java.awt.image.BufferedImage;

import estruturas.Aresta;
import estruturas.Poligono;

public class PintaPoligonoRegular {
	static BufferedImage imgS;
	static int corDaBordaS;
	static int corDePreenchimentoS;

	public static void pintaRegular(BufferedImage img, int corDaBorda,int corDePeenchimento, Poligono p) {
		imgS = img;
		corDaBordaS = corDaBorda;
		corDePreenchimentoS = corDePeenchimento;
		Aresta arestas[] = p.getArestas();
		for (int i = 0; i < arestas.length; i++) {
			DesenhaLinha.desenhaLina(arestas[i], img, corDaBorda);
		}
		int x = (int) Math.round(p.getVerticeCentral().getX());
		int y = (int) Math.round(p.getVerticeCentral().getY());

		try {
			imgS.setRGB(x, y, corDePreenchimentoS);
			

			pintaC(x, y);
			pintaB(x, y);
			pintaE(x, y);
			pintaD(x, y);
			
			pintaEC(x, y);
			pintaDC(x, y);
			pintaEB(x, y);
			pintaDB(x, y);
		} catch (Exception e) {
			// TODO: handle exception
		}
		

	}

	public static void pintaC(int x, int y) {
		y--;
		try {
			while (imgS.getRGB(x, y) != corDaBordaS) {
			imgS.setRGB(x, y, corDePreenchimentoS);
			y--;
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	public static void pintaB(int x, int y) {
		y++;
		try {
			while (imgS.getRGB(x, y) != corDaBordaS) {
			imgS.setRGB(x, y, corDePreenchimentoS);
			y++;
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	public static void pintaD(int x, int y) {
		x++;
		try {
					while (imgS.getRGB(x, y) != corDaBordaS) {
			imgS.setRGB(x, y, corDePreenchimentoS);
			x++;
		}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static void pintaE(int x, int y) {
		x--;
		try {
					while (imgS.getRGB(x, y) != corDaBordaS) {
			imgS.setRGB(x, y, corDePreenchimentoS);
			x--;
		}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static void pintaDC(int x, int y) {
		x++;
		y--;
		//imgS.setRGB(x, y, corS);
		x++;
		y--;
		int cont=1;
		try {
			while (imgS.getRGB(x, y) != corDaBordaS&&imgS.getRGB(x-1, y) != corDaBordaS) {
			cont++;			
			x++;
			y--;
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		x-=cont;
		y+=cont;
		for (int i = 0; i < cont; i++) {
			imgS.setRGB(x, y, corDePreenchimentoS);
			pintaC(x, y);
			pintaD(x, y);
			x++;
			y--;
		}
	}

	public static void pintaEC(int x, int y) {
		x--;
		y--;
		//imgS.setRGB(x, y, corS);
		x--;
		y--;
		int cont=1;
		try {
			while (imgS.getRGB(x, y) != corDaBordaS&&imgS.getRGB(x+1, y) != corDaBordaS) {
			cont++;			
			x--;
			y--;
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		x+=cont;
		y+=cont;
		for (int i = 0; i < cont; i++) {
			imgS.setRGB(x, y, corDePreenchimentoS);
			pintaC(x, y);
			pintaE(x, y);
			x--;
			y--;
		}
	}

	public static void pintaDB(int x, int y) {
		x++;
		y++;
		//imgS.setRGB(x, y, corS);
		x++;
		y++;
		int cont=1;
		try {
			while (imgS.getRGB(x, y) != corDaBordaS&&imgS.getRGB(x-1, y) != corDaBordaS) {
			cont++;			
			x++;
			y++;
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		x-=cont;
		y-=cont;
		for (int i = 0; i < cont; i++) {
			imgS.setRGB(x, y, corDePreenchimentoS);
			pintaB(x, y);
			pintaD(x, y);
			x++;
			y++;
		}
	}

	public static void pintaEB(int x, int y) {
		x--;
		y++;
		//imgS.setRGB(x, y, corS);
		x--;
		y++;
		int cont=1;
		try {
			while (imgS.getRGB(x, y) != corDaBordaS
					&& imgS.getRGB(x + 1, y) != corDaBordaS) {
				cont++;
				x--;
				y++;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		x+=cont;
		y-=cont;
		for (int i = 0; i < cont; i++) {
			imgS.setRGB(x, y, corDePreenchimentoS);
			pintaB(x, y);
			pintaE(x, y);
			x--;
			y++;
		}
	}
}
