package apresentavel;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import estruturas.Hexagono;
import estruturas.Poligono;
import estruturas.Vertice;

public class PainelDeControle extends JPanel {
	private static final byte TRIANGULO = 0;
	private static final byte QUADRADO = 1;
	private static final byte RETANGULO = 2;
	private static final byte HEXAGONO = 3;
	private static final byte LOSANGO = 4;
	private static final byte PENTAGONO = 5;
	
	
	
	private PainelDosDesenhos painelDosDesenhos;
	private byte tipoDePoligono=3;
	private Color corEscolhida=Color.black;
	

	/**
	 * Create the panel.
	 */
	public PainelDeControle() {
		setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(BorderFactory.createLineBorder(Color.red, 1)); 
		add(panel_1);
		panel_1.setLayout(new GridLayout(2,6));
		
		JButton btnTriangulo = new JButton("Triangulo");
		btnTriangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoDePoligono=TRIANGULO;
				painelDosDesenhos.setOperacao(PainelDosDesenhos.CRIAR);
			}
		});
		panel_1.add(btnTriangulo);
		
		JButton btnQuadrado = new JButton("Quadrado");
		btnQuadrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoDePoligono=QUADRADO;
				painelDosDesenhos.setOperacao(PainelDosDesenhos.CRIAR);
			}
		});
		panel_1.add(btnQuadrado);
		
		JButton btnPentagono = new JButton("Pentagono");
		btnPentagono.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoDePoligono=PENTAGONO;
				painelDosDesenhos.setOperacao(PainelDosDesenhos.CRIAR);
			}
		});
		panel_1.add(btnPentagono);
		
		JButton btnHexagono = new JButton("Hexagono");
		btnHexagono.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoDePoligono=HEXAGONO;
				painelDosDesenhos.setOperacao(PainelDosDesenhos.CRIAR);
			}
		});
		panel_1.add(btnHexagono);
		
		JButton btnRetangulo = new JButton("Retangulo");
		btnRetangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoDePoligono=RETANGULO;
				painelDosDesenhos.setOperacao(PainelDosDesenhos.CRIAR);
			}
		});
		panel_1.add(btnRetangulo);
		
		JButton btnLosango = new JButton("Losango");
		btnLosango.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoDePoligono=LOSANGO;
				painelDosDesenhos.setOperacao(PainelDosDesenhos.CRIAR);
			}
		});
		panel_1.add(btnLosango);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		panel_2.setLayout(new GridLayout(2, 3));
		
		JButton btnAumentar = new JButton("Aumentar");
		btnAumentar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelDosDesenhos.setOperacao(PainelDosDesenhos.AUMENTAR);
			}
		});
		panel_2.add(btnAumentar);
		
		JButton btnRotacionar = new JButton("Rotacionar");
		btnRotacionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelDosDesenhos.setOperacao(PainelDosDesenhos.ROTACIONAR);
			}
		});
		panel_2.add(btnRotacionar);
		
		JButton btnMover = new JButton("Mover");
		btnMover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelDosDesenhos.setOperacao(PainelDosDesenhos.MOVER);
			}
		});
		panel_2.add(btnMover);
		
		JButton btnEditarCor = new JButton("Editar Cor ");
		btnEditarCor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelDosDesenhos.setOperacao(PainelDosDesenhos.EDITARCORPREENCHIMENTO);
			}
		});
		panel_2.add(btnEditarCor);
		
		JButton btnEditarCorDa = new JButton("Editar Cor da Borda");
		btnEditarCorDa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelDosDesenhos.setOperacao(PainelDosDesenhos.EDITARCORBORDA);
			}
		});
		panel_2.add(btnEditarCorDa);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				painelDosDesenhos.setOperacao(PainelDosDesenhos.REMOVER);
			}
		});
		panel_2.add(btnRemover);
		
		JPanel panel = new JPanel();
		add(panel);
		panel.setLayout(new GridLayout(2,2));
		
		JButton btnAzul = new JButton("");
		btnAzul.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				corEscolhida = Color.blue;
			}
		});
		btnAzul.setBackground(Color.BLUE);
		panel.add(btnAzul);
		
		JButton btnVermelho = new JButton("");
		btnVermelho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				corEscolhida = Color.red;
			}
		});
		btnVermelho.setBackground(Color.RED);
		panel.add(btnVermelho);
		
		JButton btnVerde = new JButton("");
		btnVerde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				corEscolhida = Color.green;
			}
		});
		btnVerde.setBackground(Color.GREEN);
		panel.add(btnVerde);
		
		JButton btnPreto = new JButton("");
		btnPreto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				corEscolhida = Color.black;
			}
		});
		btnPreto.setBackground(Color.BLACK);
		panel.add(btnPreto);

	}


	public void setPainelDosDesenhos(PainelDosDesenhos painelDosDesenhos) {
		this.painelDosDesenhos = painelDosDesenhos;
	}
	
	public Poligono	novoPoligonoRegular(Vertice verticeCentral) {
		Poligono poligono=null;
		
		if (tipoDePoligono==TRIANGULO) {
			
		} if (tipoDePoligono==QUADRADO) {
			
		}if (tipoDePoligono==RETANGULO) {
			
		} if (tipoDePoligono==PENTAGONO) {
			
		} if (tipoDePoligono==HEXAGONO) {
			poligono=new Hexagono(50, verticeCentral, Color.BLACK, corEscolhida);
		} if (tipoDePoligono==LOSANGO) {
			
		} else {

		}		
		
		return poligono;
	}


	
	public Color getCorEscolhida() {
		return corEscolhida;
	}	
}
