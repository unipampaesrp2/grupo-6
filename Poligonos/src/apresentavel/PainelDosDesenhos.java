package apresentavel;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import desenhos.PintaPoligonoRegular;
import estruturas.Poligono;
import estruturas.Vertice;

public class PainelDosDesenhos extends JPanel {

	public static final byte AUMENTAR = 0;
	public static final byte ROTACIONAR = 1;
	public static final byte MOVER = 2;
	public static final byte EDITARCORPREENCHIMENTO = 3;
	public static final byte EDITARCORBORDA = 4;
	public static final byte CRIAR = 5;
	public static final byte REMOVER = 6;

	private List<Poligono> poligonos;
	private PainelDeControle painelDeControle;

	private MouseListener mouseListener;
	private MouseMotionListener mouseMotionListener;
	private boolean mouseListenerSetado;
	private boolean mouseMotionListenerSetado;

	/**
	 * Create the panel.
	 */
	public PainelDosDesenhos(PainelDeControle painelDeControle) {
		poligonos = new ArrayList<Poligono>();
		this.painelDeControle = painelDeControle;
		// addMouseListener(new CriaPoligono());
	}

	public void setOperacao(byte operacao) {
		if (mouseListenerSetado) {
			removeMouseListener(mouseListener);
			mouseListenerSetado = false;
		}
		if (mouseMotionListenerSetado) {
			removeMouseMotionListener(mouseMotionListener);
			mouseListenerSetado = false;
		}

		switch (operacao) {
		case AUMENTAR:
			Aumentar editaTamanho=new Aumentar();
			AcionaArrastador acionadorT=new AcionaArrastador(editaTamanho, editaTamanho);
			mouseListener=acionadorT;
			mouseListenerSetado=true;
			addMouseListener(acionadorT);
			
			break;
		case ROTACIONAR:
			Rotacionar rotacionar=new Rotacionar();
			AcionaArrastador acionadorR=new AcionaArrastador(rotacionar, rotacionar);
			mouseListener=acionadorR;
			mouseListenerSetado=true;
			addMouseListener(acionadorR);
			break;
		case MOVER:
			Mover mover=new Mover();
			AcionaArrastador acionadorM=new AcionaArrastador(mover, mover);
			mouseListener=acionadorM;
			mouseListenerSetado=true;
			addMouseListener(acionadorM);

			break;
		case EDITARCORPREENCHIMENTO:
			mouseListener = new EditaCorDePreenchimento();
			mouseListenerSetado = true;
			addMouseListener(mouseListener);
			break;
		case EDITARCORBORDA:
			mouseListener = new EditaCorDaBorda();
			mouseListenerSetado = true;
			addMouseListener(mouseListener);
			break;
		case CRIAR:
			mouseListener = new CriaPoligono();
			mouseListenerSetado = true;
			addMouseListener(mouseListener);

			break;
		case REMOVER:
			mouseListener = new RemovePoligono();
			mouseListenerSetado = true;
			addMouseListener(mouseListener);
			break;
		default:
			break;
		}
	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		BufferedImage img = new BufferedImage(1400, 800,
				BufferedImage.TYPE_INT_ARGB);
		for (int i = 0; i < poligonos.size(); i++) {
			PintaPoligonoRegular.pintaRegular(img, poligonos.get(i)
					.getCorDaBorda().getRGB(), poligonos.get(i)
					.getCorDePreenchimento().getRGB(), poligonos.get(i));
		}

		g.drawImage((Image) img, 0, 0, 1400, 800, null);
	}

	private class CriaPoligono implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			poligonos.add(painelDeControle.novoPoligonoRegular(new Vertice(e
					.getX(), e.getY())));
			revalidate();
			repaint();
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	private class RemovePoligono implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent arg0) {
			for (int i = 0; i < poligonos.size(); i++) {
				if (poligonos.get(i).estaDentro(
						new Vertice(arg0.getX(), arg0.getY()))) {
					poligonos.remove(poligonos.get(i));
					break;
				}
			}
			revalidate();
			repaint();

		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

	private class EditaCorDaBorda implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent arg0) {
			for (int i = 0; i < poligonos.size(); i++) {
				if (poligonos.get(i).estaDentro(
						new Vertice(arg0.getX(), arg0.getY()))) {
					poligonos.get(i).setCorDaBorda(
							painelDeControle.getCorEscolhida());
					break;
				}
			}
			revalidate();
			repaint();

		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

	private class EditaCorDePreenchimento implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent arg0) {
			for (int i = 0; i < poligonos.size(); i++) {
				if (poligonos.get(i).estaDentro(
						new Vertice(arg0.getX(), arg0.getY()))) {
					poligonos.get(i).setCorDePreenchimento(
							painelDeControle.getCorEscolhida());
					break;
				}
			}
			revalidate();
			repaint();

		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

	private class AcionaArrastador implements MouseListener {
		private MouseMotionListener arrastador;
		private EscolhePoligono escolheArrastado;

		public AcionaArrastador(MouseMotionListener arrastador,
				EscolhePoligono escolheArrastado) {
			super();
			this.arrastador = arrastador;
			this.escolheArrastado = escolheArrastado;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			for (int i = 0; i < poligonos.size(); i++) {
				if (poligonos.get(i).estaDentro(
						new Vertice(e.getX(), e.getY()))) {
					
					
					escolheArrastado.setPoligono(poligonos.get(i),e.getX(), e.getY());
					
					mouseMotionListener = arrastador;
					mouseMotionListenerSetado = true;
					addMouseMotionListener(arrastador);
					break;
				}
			}

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if (mouseMotionListenerSetado) {
				removeMouseMotionListener(arrastador);
				mouseMotionListenerSetado=false;
			}

		}

	}

	private class Mover implements MouseMotionListener, EscolhePoligono{
		
		private Poligono p;	
		private int x;
		private int y;
		
		
		@Override
		public void setPoligono(Poligono p, int xO, int yO) {
			this.p=p;
			x=xO;
			y=yO;
					
		}

		@Override
		public void mouseDragged(MouseEvent arg0) {
			p.moverHorizontalmente(arg0.getX()-x);
			x=arg0.getX();
			
			p.moverVericalmente(arg0.getY()-y);
			y=arg0.getY();
			
			revalidate();
			repaint();
			
		}

		@Override
		public void mouseMoved(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private class Aumentar implements MouseMotionListener, EscolhePoligono{

		private Poligono p;	
		private double ultimaDistancia;
		
		
		@Override
		public void setPoligono(Poligono p, int xO, int yO) {
			this.p=p;
			
			double vX=xO-p.getVerticeCentral().getX();
			double vY=yO-p.getVerticeCentral().getY();
			
			if (vX<0) {
				vX=0-vX;
			}
			if (vY<0) {
				vY=0-vY;
			}
			ultimaDistancia=Math.sqrt((vX*vX)+(vY*vY));
					
		}

		@Override
		public void mouseDragged(MouseEvent arg0) {
			double vX=arg0.getX()-p.getVerticeCentral().getX();
			double vY=arg0.getY()-p.getVerticeCentral().getY();
			
			if (vX<0) {
				vX=0-vX;
			}
			if (vY<0) {
				vY=0-vY;
			}
			double distanciaAtual=Math.sqrt((vX*vX)+(vY*vY));
			
			p.editarTamanho((distanciaAtual-ultimaDistancia));
			ultimaDistancia=distanciaAtual;
			System.out.println("distancia atual="+distanciaAtual+" | ultimadistancia="+ultimaDistancia+" | almentando"+(distanciaAtual-ultimaDistancia)*0.2);
			
			revalidate();
			repaint();			
		}

		@Override
		public void mouseMoved(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private class Rotacionar implements MouseMotionListener, EscolhePoligono{
		
		private Poligono p;
		private double ultimaRotacao;		
		
		@Override
		public void setPoligono(Poligono p, int xO, int yO) {
			this.p=p;
			
			if(xO>=Math.round(p.getVerticeCentral().getX())){
				if (xO-Math.round(p.getVerticeCentral().getX())==0) {
					ultimaRotacao=0;
				}else {
					ultimaRotacao=Math.atan((yO-Math.round(p.getVerticeCentral().getY()))/(xO-Math.round(p.getVerticeCentral().getX())));
				}
				
			} else{
				if ((0-(xO-Math.round(p.getVerticeCentral().getX())))==0) {
					ultimaRotacao=Math.PI;
				}else {
					double aux=Math.atan((yO-Math.round(p.getVerticeCentral().getY()))/(0-(xO-Math.round(p.getVerticeCentral().getX()))));
					ultimaRotacao=Math.PI-aux;
				}
				
			}
			
			
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			double rotacaoAtual;
			double vX=e.getX()-Math.round(p.getVerticeCentral().getX());
			double vY=e.getY()-Math.round(p.getVerticeCentral().getY());
			if(e.getX()>=Math.round(p.getVerticeCentral().getX())){
				if (e.getX()-Math.round(p.getVerticeCentral().getX())==0) {
					rotacaoAtual=0;
				}else {
					double tangente=(vY/vX);
					System.out.println("tangente="+tangente);
					rotacaoAtual=Math.atan(tangente);
				}
				
			} else{
				if ((0-(e.getX()-Math.round(p.getVerticeCentral().getX())))==0) {
					rotacaoAtual=Math.PI;
				}else {					
					double aux=Math.atan(vY/(0-vX));
					rotacaoAtual=Math.PI-aux;
				}
				
			}
			
			p.rotacionar(rotacaoAtual-ultimaRotacao);
			ultimaRotacao=rotacaoAtual;
			
			revalidate();
			repaint();
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
}
