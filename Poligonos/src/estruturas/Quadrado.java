package estruturas;

import java.awt.Color;
/**
 * 
 * @author Abner
 *
 */
public class Quadrado extends PoligonoRegular {
/**
 * 
 * @param lado
 * @param vertice
 * @param corDaBorda
 * @param corDePreenchimento
 */
	public Quadrado(double lado,Vertice vertice,Color corDaBorda, Color corDePreenchimento) {
		super(4,lado,vertice,corDaBorda,corDePreenchimento);
	}
}