package estruturas;

import java.awt.Color;

public class Hexagono extends PoligonoRegular{
	
	/**
	 * Constroi um hexagono baseado no tamanho do lado e ponto central, com a cor da borda e de preenchimento
	 * @param lado Comprimento do lado
	 * @param vertice Ponto central
	 * @param corDaBorda Cor da borda
	 * @param corDePreenchimento Cor de preenchimento
	 */
	public Hexagono(double lado,Vertice vertice,Color corDaBorda, Color corDePreenchimento) {
		super(6,lado,vertice,corDaBorda,corDePreenchimento);
	}
}
