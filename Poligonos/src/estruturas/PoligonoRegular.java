package estruturas;


import java.awt.Color;
import java.awt.image.BufferedImage;

import determinalinhas.DeterminaLinha;
import determinalinhas.LePontoDaLinha;

public class PoligonoRegular extends Poligono{
	private double rotacao;
	private double lado;
	private Vertice vertice;
	
	/**
	 * Controi um poligono regular baseado no tamanho do lado, numero de lados, e ponto central, com a cor da borda e de preenchimento
	 * @param nLados Numero de lados do poligono
	 * @param lado Comprimento do lado
	 * @param vertice Ponto central
	 * @param corDaBorda Cor da borda
	 * @param corDePreenchimento Cor de preenchimento
	 */
	public PoligonoRegular(int nLados,double lado,Vertice vertice,Color corDaBorda, Color corDePreenchimento){
		super(new Vertice[nLados]);
		this.vertice=vertice;
		super.corDaBorda=corDaBorda;
		super.corDePreenchimento=corDePreenchimento;
		this.lado=lado;
		
		//vertices=new Vertice[nLados];
		double raio=lado/(2*Math.sin(Math.PI/nLados));
		//obtem rota��o
		rotacao=((3.0/2.0)*Math.PI)-(Math.PI/nLados);
		double rotacaoAtual=rotacao;
		//cria verices
		for (int i = 0; i < vertices.length; i++) {
			vertices[i]=new Vertice(vertice.getX()+(Math.cos(rotacaoAtual)*raio),vertice.getY()+(Math.sin(rotacaoAtual)*raio));
			rotacaoAtual+=2*Math.PI/nLados;
		}
		//cria arestas
		arestas=determinaArestas(vertices);		
	}
	//reescrito 
	@Override
	public void rotacionar(double rad){
		rotacao+=rad;
		double rotacaoAtual=rotacao;
		double raio=lado/(2*Math.sin(Math.PI/vertices.length));
		for (int i = 0; i < vertices.length; i++) {
			vertices[i].setX(vertice.getX()+(Math.cos(rotacaoAtual)*raio));
			vertices[i].setY(vertice.getY()+(Math.sin(rotacaoAtual)*raio));
			rotacaoAtual+=2*Math.PI/vertices.length;
		}
	}
	/**
	 * Edita tamanho do poligono
	 * @param val Valor a ser somado ao tamanho, caso negativo ira reduzir a figura
	 */
	public void editarTamanho(double val){
		lado+=val;
		rotacionar(0);		
	}
	
	public Vertice getVerticeCentral() {
		return vertice;
	}
	public void moverVericalmente(double y) {
		vertice.setY(vertice.getY()+y);
//		super.moverVericalmente(y);
		for (int i = 0; i < vertices.length; i++) {
			vertices[i].setY(vertices[i].getY()+y);
		}
	}
	public void moverHorizontalmente(double x) {
		vertice.setX(vertice.getX()+x);
		for (int i = 0; i < vertices.length; i++) {
			vertices[i].setX(vertices[i].getX()+x);
		}
	}

	
	/**
	 * Obtem perimetro do poligono
	 * @return Perimetro
	 */
	public double getPerimetro() {
		return lado*vertices.length;
	}
	/**
	 * Obtem area 
	 * @return Area do poligono
	 */
	public double getArea() {
		return (vertices.length*lado*lado)/(4*Math.tan(Math.PI/vertices.length));
	}
	@Override
	public boolean estaDentro(Vertice ponto) {

		int maiorX = (int) Math.round(vertices[0].getX());
		int menorX = (int) Math.round(vertices[0].getX());

		int maiorY = (int) Math.round(vertices[0].getY());
		int menorY = (int) Math.round(vertices[0].getY());

		for (int i = 0; i < vertices.length; i++) {
			if (vertices[i].getX() > maiorX) {
				maiorX = (int) Math.round(vertices[i].getX());
			} else if (vertices[i].getX() < menorX) {
				menorX = (int) Math.round(vertices[i].getX());
			}

			if (vertices[i].getY() > maiorY) {
				maiorY = (int) Math.round(vertices[i].getY());
			} else if (vertices[i].getY() < menorY) {
				menorY = (int) Math.round(vertices[i].getY());
			}
		}
		
		if (ponto.getX()>maiorX||ponto.getX()<menorX||ponto.getY()>maiorY||ponto.getY()<menorY) {
			return false;
		}else {
//			boolean matriz[][]=new boolean[maiorY-menorY+1][maiorX-menorX+1];
//			final int menorYF=menorY;
//			final int menorXF=menorX;
//			for (int i = 0; i < arestas.length; i++) {
//				DeterminaLinha.desenhaLinha(new LePontoDaLinha() {					
//					@Override
//					public void pinta(int x, int y) {
//						matriz[y-menorYF][x-menorXF]=true;						
//					}
//				}, arestas[i]);
//			}
//			for (int x = (int)ponto.getX()-menorX; x >= 0; x--) {
//				if (matriz[(int)ponto.getY()-menorY][x]) {
//					return true;
//				}
//			}
//			return false;
			return true;
		}
	}
}
