package estruturas;

public class Vertice {
	private double x;
	private double y;

	/**
	 * controi o vertice com base nas coordenadas x e y.
	 * 
	 * @param x
	 * @param y
	 */
	public Vertice(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * retorna o valor x do vertice.
	 * 
	 * @return
	 */
	public double getX() {
		return x;
	}

	/**
	 * retorna o valor y do vertice.
	 * 
	 * @return
	 */
	public double getY() {
		return y;
	}

}
