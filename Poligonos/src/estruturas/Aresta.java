package estruturas;


public class Aresta {
	private Vertice v1, v2;

	/**
	 * constroi a aresta com base nos vertices fornecido pela classe Vertice.
	 * 
	 * @param v1
	 * @param v2
	 */
	public Aresta(Vertice v1, Vertice v2) {
		this.v1 = v1;
		this.v2 = v2;
	}

	/**
	 * retorna o primeiro vertice que compoe a aresta.
	 * 
	 * @return
	 */
	public Vertice getV1() {
		return v1;
	}

	/**
	 * retorna o segundo vertice que compoe a aresta
	 * 
	 * @return
	 */
	public Vertice getV2() {
		return v2;
	}

}