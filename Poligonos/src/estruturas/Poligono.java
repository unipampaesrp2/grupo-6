package estruturas;


import java.awt.Color;
import determinalinhas.DeterminaLinha;
import determinalinhas.LePontoDaLinha;

/**
 * 
 * @author Leonardo
 *
 */
public class Poligono {
	protected Color corDaBorda;
	protected Color corDePreenchimento;
	protected Vertice vertices[];
	protected Aresta arestas[];

	public Poligono(Vertice[] vertices) {
		this.vertices = vertices;
		if (vertices[1] != null) {
			arestas = determinaArestas(vertices);
		}

	}

	/**
	 * Determina as arestas baseadas nos vertices
	 * 
	 * @param vertices
	 *            Vertices ordenados
	 * @return Arestas do poligono
	 */
	protected static Aresta[] determinaArestas(Vertice[] vertices) {
		Aresta arestas[] = new Aresta[vertices.length];
		for (int i = 0; i < vertices.length; i++) {
			try {
				arestas[i] = new Aresta(vertices[i], vertices[i + 1]);
			} catch (Exception e) {
				arestas[i] = new Aresta(vertices[i], vertices[0]);
			}
		}
		return arestas;
	}

	public Vertice getVerticeCentral() {
		double tX = 0, tY = 0;
		for (int i = 0; i < vertices.length; i++) {
			tX += vertices[i].getX();
			tY += vertices[i].getY();
		}
		return new Vertice(tX / vertices.length, tY / vertices.length);
	}

	public void moverVericalmente(double y) {
		for (int i = 0; i < vertices.length; i++) {
			vertices[i].setY(vertices[i].getY() + y);
		}
	}

	public void moverHorizontalmente(double x) {
		for (int i = 0; i < vertices.length; i++) {
			vertices[i].setX(vertices[i].getX() + x);
		}
	}

	/**
	 * Edita tamanho do poligono
	 * 
	 * @param val
	 *            Valor a ser somado ao tamanho, caso negativo ira reduzir a
	 *            figura
	 */
	public void editarTamanho(double val) {
		System.out.println("ainda nao implemenstado para poligos nao regulares");
	}

	/**
	 * Rotaciona o poligono
	 * 
	 * @param rad
	 *            Rotacao em radiandos caso positivo rotaciona em sentido
	 *            anti-horario caso negativo rotaciona em sentido horario
	 */
	public void rotacionar(double graus) {
		System.out.println("ainda nao implemenstado para poligos nao regulares");
	}

	// gets
	/**
	 * Obtem perimetro do poligono
	 * 
	 * @return Perimetro
	 */
	public double getPerimetro() {
		System.out.println("ainda nao implemenstado para poligos nao regulares");
		return 0;
	}

	/**
	 * Obtem area
	 * 
	 * @return Area do poligono
	 */
	public double getArea() {
		System.out.println("ainda nao implemenstado para poligos nao regulares");
		return 0;
	}

	/**
	 * Obtem cor da borda
	 * 
	 * @return Cor da borda do poligono
	 */
	public Color getCorDaBorda() {
		return corDaBorda;
	}

	/**
	 * Obtem cor de preenchimento
	 * 
	 * @return Cor de preenchimento do poligono
	 */
	public Color getCorDePreenchimento() {
		return corDePreenchimento;
	}

	/**
	 * Obtem vertices do poligono
	 * 
	 * @return Vertices do poligono
	 */
	public Vertice[] getVertices() {
		return vertices;
	}

	/**
	 * Obtem arestas
	 * 
	 * @return Arestas do poligono
	 */
	public Aresta[] getArestas() {
		return arestas;
	}
	

	public void setCorDaBorda(Color corDaBorda) {
		this.corDaBorda = corDaBorda;
	}

	public void setCorDePreenchimento(Color corDePreenchimento) {
		this.corDePreenchimento = corDePreenchimento;
	}

	public boolean estaDentro(Vertice ponto) {
		int maiorX = (int) Math.round(vertices[0].getX());
		int menorX = (int) Math.round(vertices[0].getX());

		int maiorY = (int) Math.round(vertices[0].getY());
		int menorY = (int) Math.round(vertices[0].getY());

		for (int i = 0; i < vertices.length; i++) {
			if (vertices[i].getX() > maiorX) {
				maiorX = (int) Math.round(vertices[i].getX());
			} else if (vertices[i].getX() < menorX) {
				menorX = (int) Math.round(vertices[i].getX());
			}

			if (vertices[i].getY() > maiorY) {
				maiorY = (int) Math.round(vertices[i].getY());
			} else if (vertices[i].getY() < menorY) {
				menorY = (int) Math.round(vertices[i].getY());
			}
		}

		if (ponto.getX() > maiorX || ponto.getX() < menorX
				|| ponto.getY() > maiorY || ponto.getY() < menorY) {
			return false;
		} else {
			return true;
//			boolean matriz[][] = new boolean[maiorY - menorY + 1][maiorX
//					- menorX + 1];
//			final int menorYF = menorY;
//			final int menorXF = menorX;
//			for (int i = 0; i < arestas.length; i++) {
//				DeterminaLinha.desenhaLinha(new LePontoDaLinha() {
//					@Override
//					public void pinta(int x, int y) {
//						matriz[y - menorYF][x - menorXF] = true;
//					}
//				}, arestas[i]);
//			}
//			for (int x = (int) ponto.getX() - menorX; x >= 0; x--) {
//				if (matriz[(int) ponto.getY() - menorY][x]) {
//					return true;
//				}
//			}
//			return false;
		}

	}

}
