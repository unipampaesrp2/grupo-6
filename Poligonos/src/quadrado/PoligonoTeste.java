package quadrado;

import java.awt.Color;

import estruturas.Quadrado;
import estruturas.Vertice;
/**
 * 
 * @author Abner
 *
 */
public class PoligonoTeste {
	private static Quadrado q[]=new Quadrado [5];
	public static void main(String[] args) {
		double lado = 5;
		Vertice vertice = new Vertice(100, 100);
		 q[0] = new Quadrado(lado, vertice, new Color(1),
				new Color(1));
		q[0].rotacionar(Math.PI/2);
		Vertice vertices[] = q[0].getVertices();
		int nLados = 4;
		double raio = lado/(2 * Math.sin(Math.PI/nLados));
		// obtem rota��o
		double rotacao = ((3.0/2.0) * Math.PI) - (Math.PI/nLados);
		double rotacaoAtual = rotacao;
		// cria verices
		System.out.println("Seno de 270�: "+Math.sin((3.0/2.0) * Math.PI));
		System.out.println("Valor do seno de y = "+((Math.sin(rotacaoAtual))));
		System.out.println("Valor do cosseno de x = "+((Math.cos(rotacaoAtual))));
		System.out.println("Raio do circulo trigonometrico = "+raio);
		
		System.out.println();
		System.out.println();
		System.out.println("-----------------------------------");
		for (int i = 0; i < vertices.length; i++) {
			System.out.println("Vertice "+(i+1)+" Valor cosseno*raio de x = "+(Math.cos(rotacaoAtual)*raio)+" Valor seno*raio de y = "+((Math.sin(rotacaoAtual)*raio)));
			System.out.println("Vertice " + (i+1) + " valor de x = "
					+ (vertice.getX()+(Math.cos(rotacaoAtual)*raio))+" valor de y = "+(vertice.getY()+(Math.sin(rotacaoAtual)*raio)));						
			rotacaoAtual+=2*Math.PI/nLados;
		}
		System.out.println();
		System.out.println("----------------------------------------");
		System.out.println();
		for (int i = 0; i < vertices.length; i++) {
			System.out.println("Vertice "+(i+1)+" Valor de x = "+vertices[i].getX()+" Valor de y = "+vertices[i].getY());
		}
	
	}
}
