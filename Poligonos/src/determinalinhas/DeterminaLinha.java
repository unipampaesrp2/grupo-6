package determinalinhas;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import estruturas.Aresta;

public class DeterminaLinha {
	public static void desenhaLinha(LePontoDaLinha leitor,Aresta aresta) {
		
		leitor.pinta((int)Math.round(aresta.getV2().getX()), (int)Math.round(aresta.getV2().getY()));
//		System.out.println("x="+(int)Math.round(aresta.getV2().getX())+" | y="+ (int)Math.round(aresta.getV2().getY()));
		
		double vx=aresta.getV2().getX()-aresta.getV1().getX();
		double vy=aresta.getV2().getY()-aresta.getV1().getY();
		
		if(vy>=0){
			if (vx>=0) {
				double aumentoDeX;
				double aumentoDeY;
				if (vx>vy){
					aumentoDeX=1;
					aumentoDeY=vy/vx;
				}else {
					aumentoDeY=1;
					aumentoDeX=vx/vy;
				}
				
				double x=aresta.getV1().getX(),y=aresta.getV1().getY();
				vx+=x;
				vy+=y;
				while (y<=vy&&x<=vx) {
					leitor.pinta((int)Math.round(x), (int)Math.round(y));
					x+=aumentoDeX;
					y+=aumentoDeY;					
				}
				 
			} else {//vx<0
				double aumentoDeX;
				double aumentoDeY;
				if (0-vx>vy){
					aumentoDeX=-1;
					aumentoDeY=0-vy/vx;
				}else {
					aumentoDeY=1;
					aumentoDeX=vx/vy;
				}
				
				double x=aresta.getV1().getX(),y=aresta.getV1().getY();
				vx+=x;
				vy+=y;
				while (y<=vy&&x>=vx) {;
					leitor.pinta((int)Math.round(x), (int)Math.round(y));
					x+=aumentoDeX;
					y+=aumentoDeY;					
				}
			}
		}else {//vy<0
			if (vx>=0) {
				double aumentoDeX;
				double aumentoDeY;
				if (vx>0-vy){
					aumentoDeX=1;
					aumentoDeY=vy/vx;
				}else {
					aumentoDeY=-1;
					aumentoDeX=0-vx/vy;
				}
				
				double x=aresta.getV1().getX(),y=aresta.getV1().getY();
				vx+=x;
				vy+=y;
				
				while (y>=vy&&x<=vx) {
					leitor.pinta((int)Math.round(x), (int)Math.round(y));
					x+=aumentoDeX;
					y+=aumentoDeY;					
				}
				 
			} else {//vx<0
				double aumentoDeX;
				double aumentoDeY;
				if (0-vx>0-vy){
					aumentoDeX=-1;
					aumentoDeY=0-vy/vx;
				}else {
					aumentoDeY=-1;
					aumentoDeX=0-vx/vy;
				}
				
				double x=aresta.getV1().getX(),y=aresta.getV1().getY();
				vx+=x;
				vy+=y;
				while (y>=vy&&x>=vx) {
					leitor.pinta((int)Math.round(x), (int)Math.round(y));
					x+=aumentoDeX;
					y+=aumentoDeY;					
				}
			}
		}			
		
	}
	
	

	
	
	
	public static void desenhaLinhaSemVertices(LePontoDaLinha leitor,Aresta aresta) {
		
		
		
		double vx=aresta.getV2().getX()-aresta.getV1().getX();
		double vy=aresta.getV2().getY()-aresta.getV1().getY();
		
		if(vy>=0){
			if (vx>=0) {
				double aumentoDeX;
				double aumentoDeY;
				if (vx>vy){
					aumentoDeX=1;
					aumentoDeY=vy/vx;
				}else {
					aumentoDeY=1;
					aumentoDeX=vx/vy;
				}
				
				double x=aresta.getV1().getX(),y=aresta.getV1().getY();
				vx+=x;
				vy+=y;
				//pula o primeiro
				x+=aumentoDeX;
				y+=aumentoDeY;	
				//pula ultimo
				vx-=aumentoDeX*0.3;
				vy-=aumentoDeY*0.3;
				while (y<=vy&&x<=vx) {
					leitor.pinta((int)Math.round(x), (int)Math.round(y));
					x+=aumentoDeX;
					y+=aumentoDeY;					
				}
				 
			} else {//vx<0
				double aumentoDeX;
				double aumentoDeY;
				if (0-vx>vy){
					aumentoDeX=-1;
					aumentoDeY=0-vy/vx;
				}else {
					aumentoDeY=1;
					aumentoDeX=vx/vy;
				}
				
				double x=aresta.getV1().getX(),y=aresta.getV1().getY();
				vx+=x;
				vy+=y;
				//pula o primeiro
				x+=aumentoDeX;
				y+=aumentoDeY;	
				//pula ultimo
				vx-=aumentoDeX*0.3;
				vy-=aumentoDeY*0.3;
				while (y<=vy&&x>=vx) {;
					leitor.pinta((int)Math.round(x), (int)Math.round(y));
					x+=aumentoDeX;
					y+=aumentoDeY;					
				}
			}
		}else {//vy<0
			if (vx>=0) {
				double aumentoDeX;
				double aumentoDeY;
				if (vx>0-vy){
					aumentoDeX=1;
					aumentoDeY=vy/vx;
				}else {
					aumentoDeY=-1;
					aumentoDeX=0-vx/vy;
				}
				
				double x=aresta.getV1().getX(),y=aresta.getV1().getY();
				vx+=x;
				vy+=y;
				//pula o primeiro
				x+=aumentoDeX;
				y+=aumentoDeY;	
				//pula ultimo
				vx-=aumentoDeX*0.3;
				vy-=aumentoDeY*0.3;
				
				while (y>=vy&&x<=vx) {
					leitor.pinta((int)Math.round(x), (int)Math.round(y));
					x+=aumentoDeX;
					y+=aumentoDeY;					
				}
				 
			} else {//vx<0
				double aumentoDeX;
				double aumentoDeY;
				if (0-vx>0-vy){
					aumentoDeX=-1;
					aumentoDeY=0-vy/vx;
				}else {
					aumentoDeY=-1;
					aumentoDeX=0-vx/vy;
				}
				
				double x=aresta.getV1().getX(),y=aresta.getV1().getY();
				vx+=x;
				vy+=y;
				//pula o primeiro
				x+=aumentoDeX;
				y+=aumentoDeY;	
				//pula ultimo
				vx-=aumentoDeX*0.3;
				vy-=aumentoDeY*0.3;
				while (y>=vy&&x>=vx) {
					leitor.pinta((int)Math.round(x), (int)Math.round(y));
					x+=aumentoDeX;
					y+=aumentoDeY;					
				}
			}
		}			
		
	}
}
