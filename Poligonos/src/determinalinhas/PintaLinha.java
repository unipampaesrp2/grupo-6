package determinalinhas;

import java.awt.image.BufferedImage;

import estruturas.Aresta;

public class PintaLinha {
	public static void pintaLinha(Aresta aresta,BufferedImage img, int cor){
		DeterminaLinha.desenhaLinha(new LePontoDaLinha() {			
			@Override
			public void pinta(int x, int y) {
				img.setRGB(x, y, cor);
				
			}
		}, aresta);
	}
}
